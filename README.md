<img align="center" src="images/logo_space.png" height="200"/>

BRA2Cloud - a **B**rand new **R**esilient **A**gent-based **A**rchitecture for **Cloud** computing.
=============

## Project Website
http://bra2cloud.cic.unb.br

:mega: We are working on a new project website and it will be published soon. Stay connected!


## Related Projects
- [BRA2Cloud Spot Price History Monitor](https://gitlab.com/InfoKnow/CloudFogComputing/bra2cloud-spot-monitor) - A set of utilities for monitoring and save price changes data from Amazon EC2 Spot Instances.
- [BRA2Cloud GUI Web Application](https://gitlab.com/InfoKnow/CloudFogComputing/bra2cloud-gui) - The BRA2Cloud GUI module that executes users bag-of-tasks applications using our agent-based architecture.

## Contributing

You're welcome to contribute to this project! Any help is appreciated!

Feel free to report issues and open merge requests.

There are several aspects you can help on:
- Improving our code and sharing with us
- Testing our solutions
- Sharing and helping other users to use this framework
- You can suggest your desired features

Please contact us for questions or to get in touch.

## License
[Apache 2.0](http://www.apache.org/licenses/LICENSE-2.0.txt)